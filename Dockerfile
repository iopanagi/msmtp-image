FROM cern/cc7-base

# We use dnf instead of yum. Yum wasn't able to resolve the new dependencies of openstack packages.
RUN yum install -y dnf && \
    yum clean all && rm -rf /var/cache/yum

RUN dnf -y update && \
    dnf --setopt=tsflags=nodocs install -y \
    jq \
    msmtp && \
    dnf clean all && \
    rm -rf /var/cache/dnf/*

ARG DUMB_INIT_VERSION
ARG DUMB_INIT_SHA256

RUN curl -s -L -o /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v${DUMB_INIT_VERSION}/dumb-init_${DUMB_INIT_VERSION}_amd64 && \
    echo "$DUMB_INIT_SHA256  /usr/local/bin/dumb-init" | sha256sum -c - && \
    chmod +x /usr/local/bin/dumb-init

ENTRYPOINT ["/usr/local/bin/dumb-init","--"]
